#!/usr/bin/python3
# Ignacio Cruz de la Haza
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import hashlib
import os
import time

# Constantes. Dirección IP del servidor y contenido a enviar


class ClientHandler(ContentHandler):
    """
    Clase para manejar datos del cliente
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        self.etiquetas = {}

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name != "config":
            for atributo in attrs.getNames():
                self.etiquetas[name + '_' + atributo] = attrs.get(atributo, "")

    def get_tags(self):
        return self.etiquetas

    def toLog(self, archivo, info):
        """
        Abre el fichero log e introduce una linea
        de informacion con la hora actual
        """
        f = open(archivo, "a")
        info2 = info.replace("\r\n", " ")
        actual_time = time.gmtime(time.time())
        t1 = str(actual_time.tm_year) + str(actual_time.tm_mon)
        t2 = t1 + str(actual_time.tm_mday) + str(actual_time.tm_hour)
        written_time = t2 + str(actual_time.tm_min) + str(actual_time.tm_sec)
        f.write(written_time + info2 + "\r\n")
        f.close()


if __name__ == "__main__":
    """
    Programa principal
    """
    DIC = {}
    if len(sys.argv) != 4:
        sys.exit('Usage: python3 uaclient.py config method option')
    if sys.argv[2] not in ["REGISTER", "INVITE", "BYE"]:
        sys.exit('Invalid method ("REGISTER", "INVITE", "BYE")')
    parser = make_parser()
    cHandler = ClientHandler()
    parser.setContentHandler(cHandler)
    try:
        parser.parse(open(sys.argv[1]))
    except FileNotFoundError:
        sys.exit("Insert a correct config")
    CONFIG = cHandler.get_tags()

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((CONFIG['regproxy_ip'],
                              int(CONFIG['regproxy_puerto'])))
            if sys.argv[2] == 'REGISTER':
                TIME = int(sys.argv[3])
                # Lectura de la shell
                LINE = str("REGISTER sip:" + CONFIG['account_username'] + ":"
                           + CONFIG['uaserver_puerto']
                           + " SIP/2.0\r\nExpires: " + str(TIME) + "\r\n")
                my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                info = str(" Sent to " + CONFIG['regproxy_ip'] + ":"
                           + CONFIG['regproxy_puerto'] + ": " + LINE)
                ClientHandler.toLog(ClientHandler, CONFIG['log_path'], info)
                data = my_socket.recv(1024)
                info = data.decode('utf-8')
                info2 = str(" Received from " + CONFIG['regproxy_ip'] + ":"
                            + CONFIG['regproxy_puerto'] + ": " + info)
                ClientHandler.toLog(ClientHandler, CONFIG['log_path'], info2)
                print('Recibido -- ', data.decode('utf-8'))
                if info.startswith('SIP/2.0 401 Unauthorize'
                                   + 'd\r\nWWW Authenticate: Digest nonce="'):
                    palabras = info.split('"')
                    m = hashlib.md5()
                    m.update(bytes(palabras[-2], 'utf-8'))
                    m.update(bytes(CONFIG['account_passwd'], 'utf-8'))
                    contraseña = m.hexdigest()
                    LINE = str("REGISTER sip:" + CONFIG['account_username']
                               + ":" + CONFIG['uaserver_puerto']
                               + " SIP/2.0\r\nExpires: " + str(TIME)
                               + '\r\nAuthorization: Digest response="'
                               + contraseña + '"\r\n')
                    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                    info_aux = " Sent to " + CONFIG['regproxy_ip'] + ":"
                    inf = str(" Sent to " + CONFIG['regproxy_ip'] + ":"
                              + CONFIG['regproxy_puerto'] + ": " + LINE)
                    ClientHandler.toLog(ClientHandler, CONFIG['log_path'], inf)
                    data = my_socket.recv(1024)
                    inf = data.decode('utf-8')
                    in2 = str(" Received from " + CONFIG['regproxy_ip'] + ":"
                              + CONFIG['regproxy_puerto'] + ": " + inf)
                    ClientHandler.toLog(ClientHandler, CONFIG['log_path'], in2)
                    print('Recibido -- ', data.decode('utf-8'))
            elif sys.argv[2] == 'INVITE':
                LINE = str("INVITE sip:" + sys.argv[3]
                           + " SIP/2.0\r\nContent-Type: application/sdp"
                           + "\r\n\r\nv=0\r\no=" + CONFIG['account_username']
                           + " " + CONFIG['uaserver_ip']
                           + "\r\ns=misesion\r\nt=0\r\nm=audio "
                           + CONFIG['rtpaudio_puerto'] + " RTP\r\n")
                my_socket.send(bytes(LINE, 'utf-8'))
                info = str(" Sent to " + CONFIG['regproxy_ip'] + ":"
                           + CONFIG['regproxy_puerto'] + ": " + LINE)
                ClientHandler.toLog(ClientHandler, CONFIG['log_path'], info)
                data = my_socket.recv(1024)
                info = data.decode('utf-8')
                if info.startswith(str("SIP/2.0 40")):
                    inf = "Error: " + info
                    ClientHandler.toLog(ClientHandler, CONFIG['log_path'], inf)
                    print(inf)
                    sys.exit()
                info2 = str(" Received from " + CONFIG['regproxy_ip'] + ":"
                            + CONFIG['regproxy_puerto'] + ": " + info)
                ClientHandler.toLog(ClientHandler, CONFIG['log_path'], info2)
                palabras = info.split()
                DIC['rtpaudio'] = palabras[-2]
                DIC['rtp_ip'] = palabras[-6]
                print('Recibido -- ', data.decode('utf-8'))
                if info.startswith(str("SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 "
                                       + "Ringing\r\n\r\nSIP/2.0 200 OK\r\n")):
                    LINE = 'ACK sip:' + sys.argv[3] + ' SIP/2.0\r\n'
                    print("Enviando: " + LINE)
                    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                    inf = str(" Sent to " + CONFIG['regproxy_ip'] + ":"
                              + CONFIG['regproxy_puerto'] + ": " + LINE)
                    ClientHandler.toLog(ClientHandler, CONFIG['log_path'], inf)
                    fichero_audio = CONFIG['audio_path']
                    aEjecutar = str("./mp32rtp -i " + DIC['rtp_ip']
                                    + " -p " + DIC['rtpaudio'])
                    aEjecutar += " < " + fichero_audio
                    print("Vamos a ejecutar", aEjecutar)
                    os.system(aEjecutar)
            elif sys.argv[2] == 'BYE':
                LINE = "BYE sip:" + sys.argv[3] + " SIP/2.0\r\n"
                my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                info = str(" Sent to " + CONFIG['regproxy_ip'] + ":"
                           + CONFIG['regproxy_puerto'] + ": " + LINE)
                ClientHandler.toLog(ClientHandler, CONFIG['log_path'], info)
                data = my_socket.recv(1024)
                decodif = data.decode('utf-8')
                info = str(" Received from " + CONFIG['regproxy_ip'] + ":"
                           + CONFIG['regproxy_puerto'] + ": " + decodif)
                ClientHandler.toLog(ClientHandler, CONFIG['log_path'], info)
                print('Recibido -- ', data.decode('utf-8'))

    except ConnectionRefusedError:
        sys.exit("20101018160243 Error: No server listening at "
                 + CONFIG['regproxy_ip'] + " port "
                 + CONFIG['regproxy_puerto'])
    print("Socket terminado.")
