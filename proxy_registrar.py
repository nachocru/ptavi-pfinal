#!/usr/bin/python3
# Ignacio Cruz de la Haza
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import json
import socket
import socketserver
import sys
import time
from uaclient import ClientHandler
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import random
import hashlib


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    SIP Register Handler class
    """
    dic = {}
    DIC_NUMBER = {}

    def json2registered(self):
        """
        Comprueba si el fichero registered.json
        ya existe y reutiliza el diccionario
        """
        try:
            with open(CONFIG['database_path']) as file:
                self.dic = json.load(file)
        except FileNotFoundError:
            pass

    def comparacion(self):
        """
        Comprueba si alguno de los
        usuarios tiene el tiempo expirado
        y lo borra
        """
        lista_temporal = []
        for user in self.dic:
            actual_time = time.strftime('%Y-%m-%d %H:%M:%S %z',
                                        time.gmtime(time.time()))
            actual_time2 = time.strptime(actual_time, '%Y-%m-%d %H:%M:%S %z')
            if actual_time2 > time.strptime(self.dic[user]['expires'],
                                            '%Y-%m-%d %H:%M:%S %z'):
                lista_temporal.append(user)
        for user in lista_temporal:
            del self.dic[user]

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        dic2 = {}
        dic3 = {}
        metodos = {'REGISTER', 'INVITE', 'BYE', 'ACK'}
        self.json2registered()
        enviado = self.rfile.read()
        message = enviado.decode('utf-8')
        palabras = message.split()
        palabras2 = palabras[1].split(':')
        user = palabras2[-2]
        print('Recibido -- ', enviado.decode('utf-8'))
        if palabras[0] in metodos:
            if palabras[0] == "REGISTER":
                info = str(" Received from " + self.client_address[0] + ":"
                           + palabras2[-1] + ": " + message)
                ClientHandler.toLog(ClientHandler, CONFIG['log_path'], info)
                if len(palabras) == 5:
                    self.NUMBER = str(random.randint(0, 10))
                    dic3['number'] = self.NUMBER
                    self.DIC_NUMBER[user] = dic3
                    LINE = str("SIP/2.0 401 Unauthorized\r\nWWW Authenticate:"
                               + "Digest nonce=" + self.NUMBER + "\r\n")
                    inf = str(" Sent to " + self.client_address[0] + ":"
                              + palabras2[-1] + ": " + LINE)
                    ClientHandler.toLog(ClientHandler, CONFIG['log_path'], inf)
                    line_aux = str('SIP/2.0 401 Unauthorized\r\nWWW Authen'
                                   + 'ticate: Digest nonce="'
                                   + self.NUMBER + '"\r\n\r\n')
                    self.wfile.write(bytes(line_aux, 'utf-8'))
                elif len(palabras) == 8:
                    dic_passwd = {}
                    with open(CONFIG['database_passwdpath']) as file:
                        dic_passwd = json.load(file)
                    palabras3 = palabras[-1].split('"')
                    contraseña = palabras3[-2]
                    current_passwd = dic_passwd[user]['passwd']
                    m = hashlib.md5()
                    numero = self.DIC_NUMBER[user]['number']
                    m.update(bytes(numero, 'utf-8'))
                    m.update(bytes(current_passwd, 'utf-8'))
                    current_hash = m.hexdigest()
                    if current_hash == contraseña:
                        dic2['address'] = self.client_address[0]
                        port = palabras2[-1]
                        dic2['port'] = port
                        if int(palabras[-4]) == 0:
                            del self.dic[user]
                            self.register2json()
                        else:
                            act_time = time.strftime('%Y-%m-%d %H:%M:%S %z',
                                                     time.gmtime(time.time()))
                            expires = time.time() + int(palabras[-4])
                            t_expires = time.strftime('%Y-%m-%d %H:%M:%S %z',
                                                      time.gmtime(expires))
                            dic2['time'] = act_time
                            dic2['expires'] = t_expires
                            self.dic[user] = dic2
                            self.comparacion()
                            self.register2json()
                            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                else:
                    inf = " SIP/2.0 400 Bad Request\r\n\r\n"
                    ClientHandler.toLog(ClientHandler, CONFIG['log_path'], inf)
                    self.wfile.write(bytes(inf, 'utf-8'))
                    print(inf)
            else:
                info = str(" Received from " + self.client_address[0] + ":"
                           + str(self.client_address[1]) + ": " + message)
                ClientHandler.toLog(ClientHandler, CONFIG['log_path'], info)
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                    palabras2 = palabras[1].split(':')
                    usuario = palabras2[-1]
                    if usuario in self.dic:
                        address = self.dic[usuario]['address']
                        sock.connect((address, int(self.dic[usuario]['port'])))
                        LINE = message
                        sock.send(bytes(LINE, 'utf-8'))
                        info = str(" Sent to " + self.dic[usuario]['address']
                                   + ":" + self.dic[usuario]['port']
                                   + ": " + LINE)
                        path = CONFIG['log_path']
                        ClientHandler.toLog(ClientHandler, path, info)
                        data = sock.recv(1024)
                        print('Recibido -- ', data.decode('utf-8'))
                        self.wfile.write(data)
                    else:
                        path = CONFIG['log_path']
                        info = " SIP/2.0 404 User Not Found\r\n\r\n"
                        ClientHandler.toLog(ClientHandler, path, info)
                        self.wfile.write(bytes(info, 'utf-8'))
                        print(info)
        else:
            info = " SIP/2.0 405 Method Not Allowed\r\n\r\n"
            ClientHandler.toLog(ClientHandler, CONFIG['log_path'], info)
            self.wfile.write(bytes(info, 'utf-8'))
            print(info)

    def register2json(self):
        """
        Crea el fichero registered.json
        donde guarda todo nuestro diccionario
        """
        with open(CONFIG['database_path'], "w") as f:
            json.dump(self.dic, f, indent=1)


if __name__ == "__main__":
    """
    Programa principal
    """

    if len(sys.argv) != 2:
        sys.exit('Usage: python3 proxy_registrar.py config')
    parser = make_parser()
    cHandler = ClientHandler()
    parser.setContentHandler(cHandler)
    try:
        parser.parse(open(sys.argv[1]))
    except FileNotFoundError:
        sys.exit("Insert a correct config")
    CONFIG = cHandler.get_tags()
    ClientHandler.toLog(ClientHandler, CONFIG['log_path'], " Starting...")
    puerto = CONFIG['server_puerto']
    serv = socketserver.UDPServer(('', int(puerto)), SIPRegisterHandler)

    print("Server " + CONFIG['server_name'] + " listening at port " + puerto)
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        ClientHandler.toLog(ClientHandler, CONFIG['log_path'], " Finishing.")
