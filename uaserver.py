#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import os
from uaclient import ClientHandler
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import time


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    DIC = {}

    def handle(self):
        # Recibe mensajes y responde a los clientes
        dic_aux = {}

        metodos = {'INVITE', 'BYE', 'ACK'}

        RISTRA = str("SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 Ringing\r\n\r\nS"
                     + "IP/2.0 200 OK\r\nContent-Type: application/sdp"
                     + "\r\n\r\nv=0\r\no=" + CONFIG['account_username'] + " "
                     + CONFIG['uaserver_ip']
                     + "\r\ns=misesion\r\nt=0\r\nm=audio "
                     + CONFIG['rtpaudio_puerto'] + " RTP\r\n")
        enviado = self.rfile.read()
        message = enviado.decode('utf-8')
        print(message)
        palabras = message.split()
        nombre = palabras[1].split(":")
        if nombre[-1] != CONFIG['account_username']:
            inf = "SIP/2.0 400 Bad Request\r\n\r\n"
            ClientHandler.toLog(ClientHandler, CONFIG['log_path'], inf)
            self.wfile.write(bytes(inf, 'utf-8'))
            print(inf)
        else:
            info = str(" Received from " + CONFIG['regproxy_ip'] + ":"
                       + CONFIG['regproxy_puerto'] + ": " + message)
            ClientHandler.toLog(ClientHandler, CONFIG['log_path'], info)
            print('Recibido -- ', enviado.decode('utf-8'))
            if palabras[0] in metodos:
                if palabras[0] == 'INVITE':
                    self.DIC['rtpaudio'] = palabras[-2]
                    self.DIC['rtp_ip'] = palabras[7]
                    self.wfile.write(bytes(RISTRA, 'utf-8'))
                elif palabras[0] == 'ACK':
                    fichero_audio = CONFIG['audio_path']
                    aEjecutar = str("./mp32rtp -i " + self.DIC['rtp_ip']
                                    + " -p " + self.DIC['rtpaudio'])
                    aEjecutar += " < " + fichero_audio
                    print("Vamos a ejecutar", aEjecutar)
                    os.system(aEjecutar)
                elif palabras[0] == 'BYE':
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

            else:
                self.wfile.write(b"SIP/2.0 405 Method not Allowed\r\n\r\n")


if __name__ == "__main__":
    """
    Programa principal
    """

    if len(sys.argv) != 2:
        sys.exit('Usage: python3 uaserver.py config')
    parser = make_parser()
    cHandler = ClientHandler()
    parser.setContentHandler(cHandler)
    try:
        parser.parse(open(sys.argv[1]))
    except FileNotFoundError:
        sys.exit("Insert a correct config")
    CONFIG = cHandler.get_tags()
    ClientHandler.toLog(ClientHandler, CONFIG['log_path'], " Starting...")

    # Creamos servidor de eco y escuchamos
    puerto = int(CONFIG['uaserver_puerto'])
    serv = socketserver.UDPServer(('', puerto), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        ClientHandler.toLog(ClientHandler, CONFIG['log_path'], " Finishing.")
